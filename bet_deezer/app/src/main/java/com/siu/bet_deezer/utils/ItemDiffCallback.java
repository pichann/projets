package com.siu.bet_deezer.utils;

import android.support.v7.util.DiffUtil;

import java.util.List;

import com.siu.bet_deezer.rest.model.DeezerUser;

public class ItemDiffCallback extends DiffUtil.Callback {

    private final List<DeezerUser> oldElementList;
    private final List<DeezerUser> newElementList;

    public ItemDiffCallback(List<DeezerUser> oldElementList, List<DeezerUser> newElementList) {
        this.oldElementList = oldElementList;
        this.newElementList = newElementList;
    }

    @Override
    public int getOldListSize() {
        return oldElementList != null ? oldElementList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newElementList != null ? newElementList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        DeezerUser oldElement = oldElementList.get(oldItemPosition);
        DeezerUser newElement = newElementList.get(newItemPosition);
        return oldElement.isSameItem(newElement);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        DeezerUser oldElement = oldElementList.get(oldItemPosition);
        DeezerUser newElement = newElementList.get(newItemPosition);
        return oldElement.isSameContent(newElement);
    }
}
