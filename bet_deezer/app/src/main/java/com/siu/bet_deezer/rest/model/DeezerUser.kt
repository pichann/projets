package com.siu.bet_deezer.rest.model

import java.io.Serializable

class DeezerUser(val id: Long, val title: String, val duration: Long ,
                 val public: Boolean = true, val is_loved_track: Boolean = false, val collaborative: Boolean = false,
                 val rating: Int = 0, val nb_tracks: Int = 85, val fans: Int = 6,
                 val link: String = "https://www.deezer.com/playlist/17346287",
                 val picture: String = "https://api.deezer.com/playlist/17346287/image",
                 val picture_small: String = "https://e-cdns-images.dzcdn.net/images/cover/d910a6585e4a80f06e6fddce4f6f859d/56x56-000000-80-0-0.jpg",
                 val picture_medium: String = "https://e-cdns-images.dzcdn.net/images/cover/d910a6585e4a80f06e6fddce4f6f859d/250x250-000000-80-0-0.jpg",
                 val picture_big: String = "https://e-cdns-images.dzcdn.net/images/cover/d910a6585e4a80f06e6fddce4f6f859d/500x500-000000-80-0-0.jpg",
                 val picture_xl: String = "https://e-cdns-images.dzcdn.net/images/cover/d910a6585e4a80f06e6fddce4f6f859d/1000x1000-000000-80-0-0.jpg",
                 val checksum: String = "dd538dcf2a8f73d5425fd4e3927aed1a",
                 val tracklist: String = "https://api.deezer.com/playlist/17346287/tracks",
                 val creation_date: String = "0000-00-00 00:00:00", val type: String = "playlist",
                 val creator: Creator): Serializable {

    private val serialVersionUID = 198239821312123L


    fun isSameItem(element: DeezerUser): Boolean {
        return id  == (element.id) && title == (element.title)
    }

    fun isSameContent(element: DeezerUser): Boolean {
        return true
    }
}