package com.siu.bet_deezer.rest.model

class Artiste (val id: String =  "4341930",
               val name: String =  "ASAP Ferg",
               val link: String =  "https://www.deezer.com/artist/4341930",
               val picture: String =  "https://api.deezer.com/artist/4341930/image",
               val picture_small: String =
                   "https://e-cdns-images.dzcdn.net/images/artist/bd999c69da0beb25e47e1bbc0dca6fce/56x56-000000-80-0-0.jpg",
               val picture_medium: String =
                   "https://e-cdns-images.dzcdn.net/images/artist/bd999c69da0beb25e47e1bbc0dca6fce/250x250-000000-80-0-0.jpg",
               val picture_big: String =
                   "https://e-cdns-images.dzcdn.net/images/artist/bd999c69da0beb25e47e1bbc0dca6fce/500x500-000000-80-0-0.jpg",
               val picture_xl: String =
                   "https://e-cdns-images.dzcdn.net/images/artist/bd999c69da0beb25e47e1bbc0dca6fce/1000x1000-000000-80-0-0.jpg",
               val tracklist: String =  "https://api.deezer.com/artist/4341930/top?limit=50",
               val type: String =  "artist") {

}
