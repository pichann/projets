package com.siu.bet_deezer.service

import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptorAuthorization: Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain?.request()

        val request = original !!. newBuilder ()
                .header("Content-Type", "application/json")
                .header("Authorization", "")
                .build()

        return chain.proceed(request)
    }
}
