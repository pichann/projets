package com.siu.bet_deezer.service.repository

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import com.siu.bet_deezer.rest.model.Track
import com.siu.bet_deezer.rest.model.data
import com.siu.bet_deezer.rxjava.RxCallBacks
import com.siu.bet_deezer.service.endpoint.ApiEndpointIn
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class CallNetworkPlayRepository(val callIn: ApiEndpointIn) {

    val playLists = ObservableArrayList<Track>()
    val isComplete = ObservableBoolean()
    var count:Int = 15
    var disposable : DisposableObserver<data<Track>>? = null

    fun startDeezerListRequest(id: Long, callBacks: RxCallBacks<List<Track>>): ApiEndpointIn {
        isComplete.set(false)

        disposable = object :DisposableObserver<data<Track>>(){
            override fun onComplete() {
                callBacks.onComplete()
            }

            override fun onError(e: Throwable?) {
                isComplete.set(false)
                callBacks.onError()
            }

            override fun onNext(t: data<Track>?) {
                //little trick to fake an lazy loading
                if (t?.data?.toTypedArray() != null) {
                    count = Math.min(count, t.data.size - 1)
                    (0..count).mapTo(playLists) { t.data[it] }

                    count += 5
                    callBacks.onNext(t.data)
                }
                isComplete.set(true)
            }
        }

        callIn.getIdTracks(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .subscribe(disposable!!)

        return callIn
    }

    fun stopRequest() {
        if (disposable?.isDisposed!!) disposable?.dispose()
    }

}
