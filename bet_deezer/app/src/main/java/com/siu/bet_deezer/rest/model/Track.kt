package com.siu.bet_deezer.rest.model

class Track () {
  val id: Long =  69991404
  val readable: Boolean =  true
  val title: String =  "Shabba"
  val title_short: String =  "Shabba"
  val title_version: String =  ""
  val link: String =  "https://www.deezer.com/track/69991404"
  val duration: Int =  275
  val rank: Int =  491213
  val explicit_lyrics: Boolean =  true
  val preview: String =  "http://e-cdn-preview-5.deezer.com/stream/57a7fa0afa4081b9f590f4151d289d3f-1.mp3"
  val time_add: String =  "1399372278"
  val artist: Artiste = Artiste()


    fun isSameItem(element: Track): Boolean {
        return id  == (element.id)
    }

    fun isSameContent(element: Track): Boolean {
        return true
    }
}
