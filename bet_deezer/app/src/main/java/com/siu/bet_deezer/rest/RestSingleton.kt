package com.siu.bet_deezer.rest

import com.siu.bet_deezer.service.repository.CallNetkortDeezerListRepository
import com.siu.bet_deezer.service.repository.CallNetworkPlayRepository
import java.io.Serializable

class RestSingleton : Serializable {

     object SingletonHelper {
        val instance = RestSingleton()
    }

    val client: RestClient

    val deezerListRepository: CallNetkortDeezerListRepository
    val playListRepository  : CallNetworkPlayRepository

    init {
        client = RestClient()
        deezerListRepository = CallNetkortDeezerListRepository(client.apiEndpointOut)
        playListRepository   = CallNetworkPlayRepository(client.apiEndpointIn)
    }

    companion object {

        private const val serialVersionUID = -2662465L

        val instance: RestSingleton
            get() = SingletonHelper.instance

    }

}
