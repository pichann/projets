package com.siu.bet_deezer.utils;

import android.animation.Animator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by pierre-emmanuel on 6/18/14.
 */
public class ViewUtils {

    public static void animate(View view, Animation animation) {
        switch (animation) {
            case MORPH:
                morph(view);
                break;
            case APEAR:
                miniMorph(view, null);
                break;
        }
    }

    public static void animate(View view, Animation animation, StringBuilder count) {
        switch (animation) {
            case MORPH:
                morph(view);
                break;
            case APEAR:
                miniMorph(view, count);
                break;
        }
    }

    private static void morph(final View view) {
        final long duration = 400;

        view.setEnabled(false);
        view.animate().scaleX(1).scaleY(1).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                view.animate().scaleX(1).scaleY(1.2f).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        view.animate().scaleX(1.2f).scaleY(0.9f).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                view.animate().scaleX(0.9f).scaleY(0.9f).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animator) {
                                        view.animate().scaleX(1).scaleY(1).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animator) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animator) {
                                                view.setEnabled(true);
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animator) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animator) {

                                            }
                                        }).start();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animator) {

                                    }
                                }).start();
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        }).start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                }).start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }

    private static void miniMorph(final View view, final StringBuilder text) {
        final long duration = 400;

        view.setEnabled(false);
        view.animate().alpha(1.0f).scaleX(0.7f).scaleY(0.7f).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

                if (view instanceof TextView && text != null)
                    ((TextView) view).setText(text);

                view.animate().scaleX(0.9f).scaleY(0.9f).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        view.animate().scaleX(1).scaleY(1).setDuration(duration / 4).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                view.setEnabled(true);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        }).start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                }).start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }


    public static Bitmap getBlurredBitmap(final View view) {
        String mPath = Environment.getExternalStorageDirectory().toString() + "/" + "youmiam_blurred.jpg";

        Bitmap bitmap;

        view.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        OutputStream fileOut;
        File imageFile = new File(mPath);

        try {
            fileOut = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (Exception e) {
//            AppLogger.INSTANCE.e("Cannot take screenshot", e);
            return null;
        }


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(imageFile), null, options);
        } catch (FileNotFoundException e) {
//            AppLogger.INSTANCE.e("Cannot find screenshot to blur", e);
            return null;
        }


        return null;
    }

//    public static float getDpToPixels(int dp) {
//        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Application.getContext().getResources().getDisplayMetrics());
//    }

    public enum Animation {
        MORPH,
        APEAR
    }



    public Drawable triangle(boolean checked){
        Path path = new Path();
        path.moveTo(0, 25);
        path.lineTo(25, 0);
        path.lineTo(25, 25);
        path.close();

        PathShape shape = new PathShape(path, 25, 25);
        ShapeDrawable drawable = new ShapeDrawable(shape);
        if (checked){
            drawable.getPaint().setColor(Color.CYAN);
        }
        else
        {
            drawable.getPaint().setColor(Color.BLACK);
        }
        return drawable;
    }
}
