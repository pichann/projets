package com.siu.bet_deezer.recordcompany

import com.siu.bet_deezer.rest.model.DeezerUser

interface RecordCompanyNavigator {
    fun startPlayList(deezerUser: DeezerUser)
}
