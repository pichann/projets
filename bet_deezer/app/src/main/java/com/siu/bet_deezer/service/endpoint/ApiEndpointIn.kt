package com.siu.bet_deezer.service.endpoint

import com.siu.bet_deezer.rest.model.Track
import com.siu.bet_deezer.rest.model.data
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiEndpointIn {

    @GET("{id}/tracks")
    fun getIdTracks(@Path("id") trackId: Long): Observable<data<Track>>

    //@GET("{id}")
    //fun getItemId(@Path("id") trackId: String): Observable<List<Item>>

}