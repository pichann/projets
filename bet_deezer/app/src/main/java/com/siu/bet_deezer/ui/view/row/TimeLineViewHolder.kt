package com.siu.oodrive.explorer.ui.view.row

import android.support.v7.widget.RecyclerView
import android.view.View
import com.siu.oodrive.explorer.ui.adapter.ItemAdapter

abstract class TimeLineViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(item: T, callback: ItemAdapter.TimeLineElementCallback<T>)
}
