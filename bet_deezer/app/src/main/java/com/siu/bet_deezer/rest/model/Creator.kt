package com.siu.bet_deezer.rest.model

import java.io.Serializable

class Creator(val id :Long = 380909, val name :String = "userName",
              val tracklist :String = "https://api.deezer.com/user/38009/flow", val type :String = "user") : Serializable {

    private val serialVersionUID = 923823982129021L

}
