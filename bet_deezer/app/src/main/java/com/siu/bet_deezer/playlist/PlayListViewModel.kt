package com.siu.bet_deezer.playlist

import android.app.Activity
import android.databinding.ObservableArrayList
import android.support.v7.util.DiffUtil
import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.rest.model.Track
import com.siu.bet_deezer.rxjava.RxCallBacks
import com.siu.bet_deezer.service.repository.CallNetworkPlayRepository
import com.siu.bet_deezer.utils.GsmUtils


class PlayListViewModel(val deezerUser: DeezerUser?, val playView: PlayListView, val playRepository: CallNetworkPlayRepository): RxCallBacks<List<Track>> {

    val playList = ObservableArrayList<Track>()
    val isLoadingSucced = playRepository.isComplete

    var title: String = ""
    var author: String = ""
    var duration: String = ""

    override fun onComplete() {
        playView.success()
    }

    override fun onError() {
        playView.failed()
    }

    override fun onNext(t: List<Track>) {
        updateListView(t)
    }

    fun updateNetworkState(activity: Activity?) {
        if (GsmUtils.isOnline(activity)) {
            startPlayListsService()

        } else {
            playView.failed()
        }
    }

    private fun startPlayListsService() {
        playRepository.startDeezerListRequest(deezerUser?.id!!,this)
    }

    fun start(activity: Activity) {
        updateNetworkState(activity)

        playView.verifyingAdapterState(playList)
        playView.verifyImageHeader(deezerUser?.picture_medium)

        title  = deezerUser?.title!!
        author = deezerUser.creator.name

        val hours = deezerUser.duration / 3600
        val minutes = (deezerUser.duration % 3600) / 60
        val seconds = deezerUser.duration % 60

        val time = StringBuilder()
        addTimeFormatted(hours, time)
        time.append(":")
        addTimeFormatted(minutes, time)
        time.append(":")
        addTimeFormatted(seconds, time)

        duration = time.toString()
    }

    fun addTimeFormatted(timeUnity: Long, time: StringBuilder) {
        if (timeUnity < 10) time.append(0)
        time.append(timeUnity)

    }

    fun destroy() {
        playRepository.stopRequest()
    }

    fun loadMore() {
        startPlayListsService()
    }

    private fun updateListView(networkList: List<Track>?) {
        val oldList = playList
        val diffResult = DiffUtil.calculateDiff(TrackDiffCallback(oldList, networkList))
        if (networkList != null) {
            playList.clear()
            playList.addAll(networkList)
        }

        playView.updateListView(diffResult, playList)
    }
}

