package com.siu.bet_deezer.playlist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.siu.bet_deezer.R
import com.siu.bet_deezer.ViewModelHolder
import com.siu.bet_deezer.rest.RestSingleton
import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.utils.ActivityUtils

class PlayListActivity: AppCompatActivity() {

    companion object {
        const val PLAY_LIST_VIEWMODEL_TAG = "PLAY_LIST_VIEWMODEL_TAG"
        const val SONG_KEY: String = "SONG_KEY"

        fun start(activity: Activity, data: DeezerUser) {
            val intent = Intent(activity, PlayListActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(SONG_KEY, data)
            intent.putExtra(SONG_KEY, data)
            activity.startActivity(intent)
        }
    }

    private var mViewModel: PlayListViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic)
        val deezer = getTrackId(intent)

        val playListFragment = findOrCreateViewFragment()

        val viewPlay = PlayListView(findViewById(R.id.contentFrame), supportActionBar)


        mViewModel = findOrCreateViewModel(viewPlay, deezer)

        viewPlay.mViewModel = mViewModel
        viewPlay.navigator = playListFragment

        playListFragment.setViewModel(mViewModel!!)
    }

    private fun findOrCreateViewFragment(): PlayListFragment {
        var playFragment: Fragment? = supportFragmentManager
                .findFragmentById(R.id.contentFrame)

        if (playFragment == null) {
            playFragment = PlayListFragment.newInstance

            // Send the task ID to the fragment
            val bundle = Bundle()
            bundle.putString(PlayListFragment.ARGUMENT_PLAY_FRAGMENT,
                    intent.getStringExtra(PlayListFragment.ARGUMENT_PLAY_FRAGMENT))
            playFragment.setArguments(bundle)

            ActivityUtils.addFragmentToActivity(supportFragmentManager,
                    playFragment, R.id.contentFrame)
        }
        return playFragment as PlayListFragment
    }



    private fun findOrCreateViewModel(viewPlay: PlayListView, deezerUser: DeezerUser?): PlayListViewModel? {
        // In a configuration change we might have a ViewModel present. It's retained using the
        // Fragment Manager.
        val retainedViewModel = (supportFragmentManager?.findFragmentByTag(PLAY_LIST_VIEWMODEL_TAG))

        if (retainedViewModel != null && (retainedViewModel as ViewModelHolder<PlayListViewModel>).viewmodel != null) {
            // If the model was retained, return it.
            return retainedViewModel.viewmodel
        } else {
            // There is no ViewModel yet, create it.
            val viewModel = PlayListViewModel(deezerUser, viewPlay, RestSingleton.instance.playListRepository)
            //viewPlay.view =
            // and bind it to this Activity's lifecycle using the Fragment Manager.
            ActivityUtils.addFragmentToActivity(
                    supportFragmentManager,
                    ViewModelHolder.createContainer(viewModel),
                    PLAY_LIST_VIEWMODEL_TAG)
            return viewModel
        }
    }


    fun getTrackId(intent: Intent): DeezerUser? {
        if (intent.hasExtra(SONG_KEY))
            return intent.getSerializableExtra(SONG_KEY) as DeezerUser
        else return null
    }

    override fun onStart() {
        super.onStart()
    }
}
