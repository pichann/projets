package com.siu.bet_deezer.utils.Diff

import android.support.v7.util.DiffUtil
import java.util.*

abstract class DiffCallBack<T> : DiffUtil.Callback() {
    protected var oldList: List<T> = ArrayList()
    protected var newList: List<T> = ArrayList()

    internal fun setData(oldList: List<T>, newList: List<T>) {
        this.oldList = oldList
        this.newList = newList
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }
}