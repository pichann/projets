package com.siu.bet_deezer.recordcompany

import android.support.v7.util.DiffUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.siu.bet_deezer.R
import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.ui.view.custom.EndlessRecyclerOnScrollListener
import com.siu.oodrive.explorer.ui.adapter.ItemAdapter
import kotlinx.android.synthetic.main.fragment_record.view.*

class RecordCompanyView(var view: View): ItemAdapter.TimeLineElementCallback<DeezerUser> {

    var mViewModel: RecordCompanyViewModel? = null
    var navigator: RecordCompanyNavigator? = null

    private var adapter : ItemAdapter<DeezerUser>? = null

    override fun onElemCLick(t: DeezerUser) {
        navigator?.startPlayList(t)
    }

    fun failed() {
        view.no_connection.animate().alpha(1f).start()
        view.no_connection.text = view.context.getString(R.string.error_reload)
        view.no_connection.setOnClickListener(click())

        view.loader.visibility = View.GONE
    }

    fun success() {
        view.loader.visibility = View.GONE
        view.textView.visibility = View.GONE
        view.no_connection.visibility = View.GONE
        view.record_grid.visibility = View.VISIBLE
    }

    fun verifyingAdapterState(itemList: ArrayList<DeezerUser>) {
        if (adapter == null) {
            adapter = ItemAdapter(itemList, this)
            view.record_grid?.adapter =  adapter
            view.record_grid?.layoutManager =  GridLayoutManager(view.context, 3)

            view.record_grid.addOnScrollListener(OnScroll(GridLayoutManager(view.context, 3), mViewModel))
        } else {
        }
    }

    fun click(): View.OnClickListener =  View.OnClickListener {
        view.loader.visibility = View.VISIBLE
        mViewModel?.updateNetworkState(view.context)
    }

    class OnScroll(l: LinearLayoutManager, val viewModel: RecordCompanyViewModel?) : EndlessRecyclerOnScrollListener(l) {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
        }

        override fun onLoadMore() {
           viewModel?.loadMore()
        }
    }

    fun updateListView(diffResult: DiffUtil.DiffResult, listItem: ArrayList<DeezerUser>?) {
        if (listItem != null) {
            verifyingAdapterState(listItem)
        }
        //adapter?.resetAnimation()
        diffResult.dispatchUpdatesTo(adapter)
        adapter?.notifyDataSetChanged()
    }
}

