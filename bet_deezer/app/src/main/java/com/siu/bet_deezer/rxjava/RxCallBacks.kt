package com.siu.bet_deezer.rxjava

interface RxCallBacks<T> {
    fun onComplete()
    fun onError()
    fun onNext(t:T)

}