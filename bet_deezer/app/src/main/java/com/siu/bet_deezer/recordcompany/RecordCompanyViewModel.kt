package com.siu.bet_deezer.recordcompany

import android.content.Context
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.support.v7.util.DiffUtil
import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.rxjava.RxCallBacks
import com.siu.bet_deezer.service.repository.CallNetkortDeezerListRepository
import com.siu.bet_deezer.utils.GsmUtils
import com.siu.bet_deezer.utils.ItemDiffCallback


class RecordCompanyViewModel(val recordView: RecordCompanyView, val deezerListRepository: CallNetkortDeezerListRepository): RxCallBacks<List<DeezerUser>> {

    val playList = ObservableArrayList<DeezerUser>()
    val isLoadingSucced = deezerListRepository.isComplete
    val string: ObservableBoolean = ObservableBoolean()


    override fun onComplete() {
        recordView.success()
    }

    override fun onError() {
        recordView.failed()
    }

    override fun onNext(t: List<DeezerUser>) {
        updateListView(t)
    }

    fun updateNetworkState(context: Context?) {
        if (GsmUtils.isOnline(context)) {
            startPlayListsService()

        } else {
            recordView.failed()
        }
    }

    private fun startPlayListsService() {
        deezerListRepository.startDeezerListRequest(this)
    }

    fun start(context: Context) {

        updateNetworkState(context)
        recordView.verifyingAdapterState(playList)
    }

    fun destroy() {
        deezerListRepository.stopRequest()
    }

    fun loadMore() {
        deezerListRepository.startDeezerListRequest(this)
    }

    private fun updateListView(listItem: List<DeezerUser>?) {
        val oldList = playList
        val diffResult = DiffUtil.calculateDiff(ItemDiffCallback(oldList, listItem))
        if (listItem != null) {
            playList.clear()
            playList.addAll(listItem)
        }

        recordView.updateListView(diffResult, playList)
    }
}

