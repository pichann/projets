package com.siu.bet_deezer.rest


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.siu.bet_deezer.service.RequestInterceptorAuthorization
import com.siu.bet_deezer.service.endpoint.ApiEndpointIn
import com.siu.bet_deezer.service.endpoint.ApiEndpointOut
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestClient {

    private val mServiceOut = ThreadLocal<ApiEndpointOut>()
    private val mServiceIn = ThreadLocal<ApiEndpointIn>()

    private val gson: Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .create()

    init {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(RequestInterceptorAuthorization())

        val rxAdapterOut = RxJava2CallAdapterFactory.create()
        val retrofitRxOut = Retrofit.Builder()
                .baseUrl("https://api.deezer.com/")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxAdapterOut)
                .build()
        mServiceOut.set(retrofitRxOut.create<ApiEndpointOut>(ApiEndpointOut::class.java))



        //RX_PART
        val rxAdapter = RxJava2CallAdapterFactory.create()
        val retrofitRx = Retrofit.Builder()
                .baseUrl("https://api.deezer.com/playlist/")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxAdapter)
                .build()
        mServiceIn.set(retrofitRx.create<ApiEndpointIn>(ApiEndpointIn::class.java))
    }

    val apiEndpointOut: ApiEndpointOut
        get() = mServiceOut.get()

    val apiEndpointIn: ApiEndpointIn
        get() = mServiceIn.get()

}
