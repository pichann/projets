package com.siu.bet_deezer.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by pierre-emmanuetouchet on 29/12/16.
 */

object GsmUtils {
    /**
     * @param context
     * *
     * @return a boolean that show-us if we have ANY internet connection
     */
    fun isOnline(context: Context?): Boolean {
        if (context == null) return false

        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}
