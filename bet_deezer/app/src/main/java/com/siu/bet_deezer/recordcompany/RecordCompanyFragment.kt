package com.siu.bet_deezer.recordcompany

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siu.bet_deezer.R
import com.siu.bet_deezer.databinding.FragmentRecordBinding
import com.siu.bet_deezer.playlist.PlayListActivity
import com.siu.bet_deezer.rest.model.DeezerUser

class RecordCompanyFragment: Fragment(), RecordCompanyNavigator {
    companion object {
        val ARGUMENT_RECORD_FRAGMENT = "RECORD_FRAGMENT_ID"

        val newInstance: RecordCompanyFragment
            get() {
                val fragment: RecordCompanyFragment = RecordCompanyFragment()
                return fragment
            }
    }

    var mViewModel: RecordCompanyViewModel? = null
    var mViewDataBinding: FragmentRecordBinding? = null

    override fun onResume() {
        super.onResume()
        mViewModel?.start(activity)
    }

    override fun onStop() {
        mViewModel?.destroy()
        super.onStop()
    }

    fun setViewModel(viewModel: RecordCompanyViewModel) {
        mViewModel = checkNotNull(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater?.inflate(R.layout.fragment_record, container, false)
        if (mViewDataBinding == null) {
            mViewDataBinding = FragmentRecordBinding.bind(root)
        }
        mViewDataBinding?.viewmodel = mViewModel

        retainInstance = false

        return root
    }

    override fun startPlayList(deezerUser: DeezerUser) {
        PlayListActivity.start(activity, deezerUser)
    }

}
