package com.siu.oodrive.explorer.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.siu.bet_deezer.R
import com.siu.oodrive.explorer.ui.view.row.FileRow
import com.siu.oodrive.explorer.ui.view.row.TimeLineViewHolder

class ItemAdapter<T>(var datas: ArrayList<T>, val callback: TimeLineElementCallback<T>) :
        RecyclerView.Adapter<TimeLineViewHolder<T>>() {

    var lastPosition = -1

    override fun getItemCount(): Int = this.datas.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TimeLineViewHolder<T> {
        val v = (LayoutInflater.from(parent?.context).inflate(R.layout.row_file, parent, false))
        return FileRow(v)
    }

    override fun onBindViewHolder(holder: TimeLineViewHolder<T>?, position: Int) {
        holder?.bind(datas[position], callback)
    }

    private fun setAnimation(viewToAnimate: View?, position: Int) {
        if (position > lastPosition && viewToAnimate != null) {
            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    interface TimeLineElementCallback<T> {
        fun onElemCLick(t: T)
    }

}

