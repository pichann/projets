/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.siu.bet_deezer.data.source;


public interface SourceParentContrat {

    interface LoadTasksCallback {

        //void onTasksLoaded(List<User> tasks);

        //void onDataNotAvailable();
    }

    interface GetTaskCallback {

        //void onTaskLoaded(User task);

//        void onDataNotAvailable();
    }

  //  void getTasks(@NonNull LoadTasksCallback callback);

    //void getTask(@NonNull String taskId, @NonNull GetTaskCallback callback);

    //void saveTask(@NonNull User task);

    //void completeTask(@NonNull User task);

//    void completeTask(@NonNull String taskId);

    //void activateTask(@NonNull User task);

  //  void activateTask(@NonNull String taskId);

//    void clearCompletedTasks();

    //void refreshTasks();

    //void deleteAllTasks();

    //void deleteTask(@NonNull String taskId);
}
