package com.siu.bet_deezer.playlist

import android.net.Uri
import android.support.v7.app.ActionBar
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.siu.bet_deezer.rest.model.Track
import com.siu.bet_deezer.ui.view.custom.EndlessRecyclerOnScrollListener
import com.siu.oodrive.explorer.ui.adapter.ItemAdapter
import kotlinx.android.synthetic.main.content_scrolling.view.*
import kotlinx.android.synthetic.main.header_track.view.*

class PlayListView (var view: View, var supportActionBar: ActionBar?): ItemAdapter.TimeLineElementCallback<Track> {

    var mViewModel: PlayListViewModel? = null
    var navigator: PlayListNavigator? = null

    private var adapter : ItemAdapter<Track>? = null

    override fun onElemCLick(track: Track) {
        navigator?.startPlayList(track)
    }

    fun failed() {
        //first Screen handle Issue (cause it's a very basic app)
    }

    fun success() {
        //first Screen handle Issue (cause it's a very basic app)
    }

    fun verifyingAdapterState(playList: ArrayList<Track>) {
       if (adapter == null) {
           adapter = ItemAdapter(playList, this)
           val LinearLayoutManager = LinearLayoutManager(view.context)
           view.record_grid.layoutManager = LinearLayoutManager
           view.record_grid.adapter = adapter

           view.record_grid.addOnScrollListener(OnScroll(LinearLayoutManager(view.context), mViewModel))

       }
    }

    fun verifyImageHeader(img: String?) {
        val uri = Uri.parse(img)
        view.bgImage.setImageURI(uri)
    }

    fun click(): View.OnClickListener =  View.OnClickListener {
        mViewModel?.updateNetworkState(null)
    }

    class OnScroll(l: LinearLayoutManager, val viewModel: PlayListViewModel?) : EndlessRecyclerOnScrollListener(l) {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
        }

        override fun onLoadMore() {
            viewModel?.loadMore()
        }
    }

    fun updateListView(diffResult: DiffUtil.DiffResult, listItem: ArrayList<Track>?) {
        if (listItem != null) {
            verifyingAdapterState(listItem)
        }

        diffResult.dispatchUpdatesTo(adapter)
        adapter?.notifyDataSetChanged()
    }
}

