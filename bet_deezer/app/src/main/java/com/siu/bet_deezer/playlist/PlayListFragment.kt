package com.siu.bet_deezer.playlist

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siu.bet_deezer.R
import com.siu.bet_deezer.databinding.FragmentPlayBinding
import com.siu.bet_deezer.rest.model.Track

class PlayListFragment: Fragment(), PlayListNavigator {
    companion object {
        val ARGUMENT_PLAY_FRAGMENT = "PLAY_FRAGMENT_ID"

        val newInstance: PlayListFragment
            get() {
                val fragment: PlayListFragment = PlayListFragment()
                return fragment
            }
    }

    var mViewModel: PlayListViewModel? = null
    var mViewDataBinding: FragmentPlayBinding? = null

    override fun onResume() {
        super.onResume()
        mViewModel?.start(activity as AppCompatActivity)
    }

    override fun onStop() {
        mViewModel?.destroy()
        super.onStop()
    }

    fun setViewModel(viewModel: PlayListViewModel) {
        mViewModel = checkNotNull(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater?.inflate(R.layout.fragment_play, container, false)
        if (mViewDataBinding == null) {
            mViewDataBinding = FragmentPlayBinding.bind(root)
        }
        mViewDataBinding?.viewmodel = mViewModel

        val toolbar = root?.findViewById(R.id.toolbar) as Toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        retainInstance = false

        return root
    }

    override fun startPlayList(track: Track) {
        Snackbar.make(this.view!!, R.string.not_implemented, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

}
