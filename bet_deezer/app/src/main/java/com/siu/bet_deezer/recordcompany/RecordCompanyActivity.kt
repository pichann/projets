package com.siu.bet_deezer.recordcompany

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.siu.bet_deezer.R
import com.siu.bet_deezer.ViewModelHolder
import com.siu.bet_deezer.rest.RestSingleton
import com.siu.bet_deezer.utils.ActivityUtils

class RecordCompanyActivity: AppCompatActivity() {

    val ADD_EDIT_VIEWMODEL_TAG = "RECORD_COMPANY_VIEWMODEL_TAG"

    private var mViewModel: RecordCompanyViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic)

        val recordFragment = findOrCreateViewFragment()


        val viewRecord = RecordCompanyView(findViewById(R.id.contentFrame))
        mViewModel = findOrCreateViewModel(viewRecord)

        viewRecord.navigator = recordFragment
        viewRecord.mViewModel = mViewModel

        recordFragment.setViewModel(mViewModel!!)
    }

    private fun findOrCreateViewFragment(): RecordCompanyFragment {
        var recordFragment: Fragment? = supportFragmentManager
                .findFragmentById(R.id.contentFrame)

        if (recordFragment == null) {
            recordFragment = RecordCompanyFragment.newInstance

            // Send the task ID to the fragment
            val bundle = Bundle()
            bundle.putString(RecordCompanyFragment.ARGUMENT_RECORD_FRAGMENT,
                    intent.getStringExtra(RecordCompanyFragment.ARGUMENT_RECORD_FRAGMENT))
            recordFragment.setArguments(bundle)

            ActivityUtils.addFragmentToActivity(supportFragmentManager,
                    recordFragment, R.id.contentFrame)
        }
        return recordFragment as RecordCompanyFragment
    }



    private fun findOrCreateViewModel(viewRecord: RecordCompanyView): RecordCompanyViewModel? {
        // In a configuration change we might have a ViewModel present. It's retained using the
        // Fragment Manager.
        val retainedViewModel = (supportFragmentManager?.findFragmentByTag(ADD_EDIT_VIEWMODEL_TAG))

        if (retainedViewModel != null && (retainedViewModel as ViewModelHolder<RecordCompanyViewModel>).viewmodel != null) {
            // If the model was retained, return it.
            return retainedViewModel.viewmodel
        } else {
            // There is no ViewModel yet, create it.
            val viewModel = RecordCompanyViewModel(viewRecord, RestSingleton.instance.deezerListRepository)
            //viewRecord.view =
            // and bind it to this Activity's lifecycle using the Fragment Manager.
            ActivityUtils.addFragmentToActivity(
                    supportFragmentManager,
                    ViewModelHolder.createContainer(viewModel),
                    ADD_EDIT_VIEWMODEL_TAG)
            return viewModel
        }
    }


}
