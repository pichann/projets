package com.siu.bet_deezer.utils.Diff;

import android.support.v7.util.DiffUtil;

import java.util.List;

import com.siu.bet_deezer.rest.model.Track;

public class TrackDiffCallback extends DiffUtil.Callback {

    private final List<Track> oldElementList;
    private final List<Track> newElementList;

    public TrackDiffCallback(List<Track> oldElementList, List<Track> newElementList) {
        this.oldElementList = oldElementList;
        this.newElementList = newElementList;
    }

    @Override
    public int getOldListSize() {
        return oldElementList != null ? oldElementList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newElementList != null ? newElementList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Track oldElement = oldElementList.get(oldItemPosition);
        Track newElement = newElementList.get(newItemPosition);
        return oldElement.isSameItem(newElement);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Track oldElement = oldElementList.get(oldItemPosition);
        Track newElement = newElementList.get(newItemPosition);
        return oldElement.isSameContent(newElement);
    }
}
