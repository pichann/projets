package com.siu.bet_deezer.playlist

import com.siu.bet_deezer.rest.model.Track

interface PlayListNavigator {
    fun startPlayList(track: Track)
}
