package com.siu.bet_deezer.service.repository

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.rest.model.data
import com.siu.bet_deezer.rxjava.RxCallBacks
import com.siu.bet_deezer.service.endpoint.ApiEndpointOut
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class CallNetkortDeezerListRepository(val call: ApiEndpointOut) {

    val playLists = ObservableArrayList<DeezerUser>()
    val isComplete = ObservableBoolean()
    var count:Int = 15
    var disposable : DisposableObserver<data<DeezerUser>>? = null

    fun startDeezerListRequest(callBacks: RxCallBacks<List<DeezerUser>>): ApiEndpointOut {
        isComplete.set(false)

        disposable = object :DisposableObserver<data<DeezerUser>>(){
            override fun onComplete() {
                callBacks.onComplete()
            }

            override fun onError(e: Throwable?) {
                isComplete.set(false)
                callBacks.onError()
            }

            override fun onNext(t: data<DeezerUser>?) {
                if (t?.data?.toTypedArray() != null) {

                    count = Math.min(count, t.data.size-1)
                    (0..count).mapTo(playLists) { t.data[it] }
                    count += 5
                    callBacks.onNext(t.data)
                }
                isComplete.set(true)
            }
        }

        call.getUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .subscribe(disposable!!)

        return call
    }

    fun stopRequest() {
        if (disposable?.isDisposed!!) disposable?.dispose()
    }

}
