package com.siu.bet_deezer.service.endpoint

import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.rest.model.data
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiEndpointOut {

    @GET("user/{userId}/playlists")
    fun getUser(@Path("userId") userId: String="3809309"): Observable<data<DeezerUser>>
}
