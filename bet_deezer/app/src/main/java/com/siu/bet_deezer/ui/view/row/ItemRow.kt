package com.siu.oodrive.explorer.ui.view.row

import android.net.Uri
import android.view.View
import com.siu.bet_deezer.rest.model.DeezerUser
import com.siu.bet_deezer.rest.model.Track
import com.siu.oodrive.explorer.ui.adapter.ItemAdapter
import kotlinx.android.synthetic.main.row_file.view.*


class FileRow<T>(val view : View) : TimeLineViewHolder<T>(view) {

    //TODO: there is an Interface needed here...
    override fun bind(item: T, callback: ItemAdapter.TimeLineElementCallback<T>) {
        view.setOnClickListener({ callback.onElemCLick(item) })
        if (item is DeezerUser) {
            view.textView.text = item.title
            val uri = Uri.parse(item.picture_big)
            view.fresco_image_musique.setImageURI(uri)

        } else if (item is Track) {
            view.textView.text = item.title
            val uri = Uri.parse(item.artist.picture_xl)
            view.fresco_image_musique.setImageURI(uri)

        }
    }

}
